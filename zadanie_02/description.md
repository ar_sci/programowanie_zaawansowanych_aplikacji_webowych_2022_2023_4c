# programowanie_zaawansowanych_aplikacji_webowych

Zadania należy zaimplementować z wykorzystaniem `node.js` oraz językiem programowania `javascript`.

---

Potrzebne pliki z danymi to:
- druzyny.txt
- sedziowie.txt
- wyniki.txt

---

W pliku **druzyny.txt** każdy wiersz danych zawiera informacje o drużynie przeciwnej:
- `Id_druzyny` => identyfikator drużyny, liczba z zakresu od 1 do 100
- `Nazwa` => nazwa drużyny, tekst o maksymalnej długości 30 znaków
- `Miasto` => miasto pochodzenia drużyny, tekst o maksymalnej długości 30 znaków

W pliku **sedziowie.txt** każdy wiersz danych zawiera informacje o jednym sędzi:
- `Nr_licencji` => numer licencji, tekst o długości 6 znaków
- `Imie` => imię sędziego, tekst o maksymalnej długości 20 znaków
- `Nazwisko` => nazwisko sędziego, tekst o maksymalnej długości 50 znaków

W pliku **wyniki.txt** każdy wiersz danych zawiera informacje o wynikach jednego meczu rozegranego przez drużynę *Galop Kucykowo*:
- `Data_meczu` => data rozegrania meczu w formacie rrrr-mm-dd
- `Rodzaj_meczu` => rodzaj meczu (T – towarzyski, L – ligowy, P – pucharowy, jeden znak)
- `Gdzie` => miejsce rozegrania meczu (W – wyjazdowy, D – u siebie, jeden znak)
- `Id_druzyny` => identyfikator drużyny przeciwnej, liczba z zakresu od 1 do 100
- `Nr_licencji` => numer licencji sędziego meczu, tekst o długości 6 znaków
- `Bramki_zdobyte` => bramki zdobyte przez Galop Kucykowo, liczba z zakresu od 0 do 20
- `Bramki_stracone` => bramki stracone przez Galop Kucykowo, liczba z zakresu od 0 do 20

---

## Zadanie_0
1. Podaj, ile towarzyskich, ile ligowych oraz ile pucharowych meczów rozegrała drużyna `Galop Kucykowo` z drużynami ze swego miasta.
1. W którym roku drużyna `Galop Kucykowo` rozegrała najwięcej meczów z drużynami ze swego miasta (łącznie wszystkie rodzaje meczów)? Podaj rok i liczbę tych meczów.

## Zadanie_1
Podaj listę zawierającą nazwy drużyn, z którymi drużyna `Galop Kucykowo` ma zerowy bilans bramkowy, tzn. łączna liczba bramek zdobytych we wszystkich meczach rozegranych z daną drużyną jest równa łącznej liczbie bramek straconych w tych meczach

## Zadanie_2 
Podaj liczby meczów wyjazdowych – wygranych, przegranych i zremisowanych – przez drużynę `Galop Kucykowo`.

## Zadanie_3
Podaj, ilu sędziów spośród tych zapisanych w pliku sedziowie.txt nie sędziowało żadnego pucharowego meczu drużyny `Galop Kucykowo`.

---

## Odpowiedzi:
```
Zadanie_0:
  1. Liczba meczy:
    T 6
    L 113
    P 25
  2.
    Rok 2007
    Liczba meczy rozegranych z drużynami ze swego miasta 21.

Zadanie_1:
  Zwinne Mewy
  Nocne Pumy

Zadanie_2:
  Przegrane:   452
  Zremisowane: 170
  Wygrane:     579

Zadanie_3:
  22
```
