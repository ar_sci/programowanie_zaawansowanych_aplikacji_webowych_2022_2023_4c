# programowanie_zaawansowanych_aplikacji_webowych

Zadania należy zaimplementować z wykorzystaniem `node.js` oraz językiem programowania `javascript`.

---

Potrzebne pliki z danymi to:
- awarie.txt
- komputery.txt
- naprawy.txt

---

## Zadanie_0
Znajdź 10 najczęstszych rodzajów dysków (czyli 10 najczęściej występujących pojemności) wśród komputerów w centrum. Dla każdej ze znalezionych pojemności podaj liczbę komputerów z takim dyskiem. Posortuj zestawienie nierosnąco względem liczby komputerów z dyskiem o danej pojemności.

## Zadanie_1
Znajdź wszystkie komputery w sekcji A, w których trzeba było przynajmniej dziesięciokrotnie wymieniać podzespoły. Podaj ich numery, a także liczbę wymian podzespołów dla każdego z nich.

## Zadanie_2 
Pewnego dnia nastąpiła awaria wszystkich komputerów w jednej z sekcji. Podaj datę awarii oraz symbol sekcji, w której nastąpiła awaria.

## Zadanie_3
Znajdź awarię, której usunięcie trwało najdłużej (czas liczymy od wystąpienia awarii do momentu zakończenia ostatniej z napraw, jakiej ta awaria wymagała). Podaj numer zgłoszenia, czas wystąpienia awarii i czas zakończenia ostatniej naprawy.

## Zadanie_4
Podaj liczbę komputerów, które nie uległy żadnej awarii o priorytecie większym lub równym 8 (wliczamy w to też komputery, które w ogóle nie uległy awarii).

---

## Odpowiedzi:
```
Zadanie_0:
  Pojemność      Liczba
    300            173
    200            31
    500            31
    800            29
    700            28
    600            26
    400            20
    290            11
    220            10
    160            10

Zadanie_1:
  Komputer       Liczba wymian podzespołów
    202            12
    123            11
    171            12
    42             11

Zadanie_2:
  Dzień: 23-12-2015
  Sekcja: Q

Zadanie_3:
  Numer zgłoszenia: 2087
  Czas wystąpienia awarii: 06-11-2015 12:38:46
  Czas zakończenia ostatniej naprawy: 13-11-2015 12:38:32

Zadanie_4:
  149
```
