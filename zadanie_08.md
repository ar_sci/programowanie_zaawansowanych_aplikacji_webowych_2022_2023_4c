# Tworzenie postów i zapisywanie ich do bazy danych

Zadanie polega na utworzeniu pola tekstowego, dzięki któremu można będzie wprowadzać tekst jako post. Pod polem tekstowym należy wyświetlić wszystkie posty, które są dodane w bazie danych (zawierają autora/użytkownika, datę dodania oraz treść).
    - jeśli nie posiadasz użytkowników, to dodaj dodatkowe pole jako autor postu
        - baza danych jest wymagana
    - jeśli posiadasz bazę danych, to zalogowany użytkownik jest autorem postu

Dodatkowo należy zaimplementować możliwość dodawania komentarzy do postów.

# Dodatkowe informacje
- od najnowszego do najstarszego
- najlepiej wyświetlać 10 ostatnich dodanych postów
     - oraz maksymalnie 3 komentarze
- przycisk, który wczyta kolejne posty
- każdy post może przekierować na osobną stronę, w której jest dodana możliwość komentowania oraz czytania wszystkich komentarzy
