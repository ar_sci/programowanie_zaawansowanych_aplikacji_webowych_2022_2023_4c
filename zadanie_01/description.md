# programowanie_zaawansowanych_aplikacji_webowych

Zadania należy zaimplementować z wykorzystaniem `node.js` oraz językiem programowania `javascript`.

---

Potrzebne pliki z danymi to:
- studenci.txt
- meldunek.txt
- wypozyczenia.txt

---

## Zadanie_0
Podaj imię i nazwisko osoby, która wypożyczyła najwięcej podręczników. Wypisz tytuły wszystkich książek przez nią wypożyczonych.

## Zadanie_1
Podaj średnią liczbę osób zameldowanych w jednym pokoju. Wynik zaokrąglij do 4 miejsc po przecinku.

## Zadanie_2 
W numerze PESEL zawarta jest informacja o płci osoby. Jeżeli przedostatnia cyfra numeru jest parzysta, to PESEL należy do kobiety, jeśli nieparzysta, to do mężczyzny. \
Podaj liczbę kobiet i liczbę mężczyzn wśród studentów.

## Zadanie_3
Podaj nazwiska i imiona studentów, którzy nie mieszkają w pokojach w miasteczku akademickim. Listę posortuj alfabetycznie wg nazwisk.

## Zadanie_4
Biblioteka planuje wprowadzenie zakazu wypożyczania kilku egzemplarzy tego samego tytułu podręcznika studentom mieszkającym w jednym pokoju. Gdy ta zasada będzie obowiązywać, w żadnym pokoju nie powtórzy się żaden tytuł podręcznika. \
Podaj, ile byłoby wypożyczonych podręczników, gdyby takie ograniczenie już funkcjonowało

---

## Odpowiedzi:
```
Zadanie_0:
  Imię i nazwisko: KRZYSZTOF LEWANDOWSKI
  Tytuły książek:
    - FLASH I PHP
    - JEZYKI PROGRAMOWANIA II
    - METODY NUMERYCZNE II
    - TEORIA GRAFOW

Zadanie_1:
  4,7101

Zadanie_2:
  Kobiety: 138
  Mężczyźni: 192

Zadanie_3:
  nazwisko           imię
    DYLAG             JACEK
    NAJDA             PIOTR
    PIETRASZEWSKI     STEFAN
    SIECZKOWSKI       MACIEJ
    ZALESKA           JULIA

Zadanie_4:
  316
```
