import logo from './logo.svg';
import './App.css';

import { useState, useRef, useEffect } from 'react';

const ComponentB = (props) => {
  return (
    <div>
      {/* ComponentB: {props.abc} */}
      ComponentB: {props["abc"]}
    </div>
  )
};

const ComponentA = () => {
  const [value, setValue] = useState(2);
  const [number, setNumber] = useState(2);
  const [arr, setArray] = useState([]);

  const inputNameRef = useRef();
  const selectOptionRef = useRef();

  useEffect(() => {
    // ...
    console.log("useEffect", value);

    return () => {
// logika kiedy komponent nam zginie (zostanie usuniety)
    };
  }, [value]);

  useEffect(() => {
    console.log("useEffect", number);
  }, [number]);

  const handlerButton = () => {
    setValue(Math.random());

    console.log(selectOptionRef.current.value);

    const refValue = inputNameRef.current.value;
    setArray([
      ...arr, refValue
    ]);

    // const copy = [...arr];
    // copy.splice(0, 1);
    // setArray(copy);
  }

  return (
    <div>
      <div>Value: {value}</div>
      <div>Numer: {number}</div>
      <input ref={inputNameRef} type='text' />
      <button onClick={handlerButton}>Przycisk</button>
      <button onClick={() => { setNumber(Math.random()) }}>Przycisk2</button>

      <select ref={selectOptionRef}>
        {
          arr.map((el, index) => {
            return (
              <option key={index}>{el}</option>
            );
          })
        }
      </select>

      <div>
        aaa
        {
          value ? "TAK" : "NIE"
        }
      </div>
      <div>
        {
          arr.map((el, index) => {
            return (
              <ComponentB abc={el} />
            );
          })
        }
      </div>
    </div>
  );
};

function App() {


  return (
    <div>
      <ComponentA />
    </div>
  );
}

export default App;
