# Zadanie

Wykorzystując framework `Angular` lub `React` należy przygotować stronę, która przechowuje dynamicznie dane, które zostaną dodane przez formularz. Strona skłąda się z dwóch paneli. Pierwszy z nich jest to panel, który umożliwi wprowadzanie danych, a drugi zaś tabelką, która wyświetla dodane dane.

## Panel 1
- zawiera 4 pola, dzięki którym można wprowadzić dane:
    - imię
    - nazwisko
    - wybór prefixu numeru telefonu (polski: +48, niemiecki: +49, usa: +1)
    - numer telefonu
- każde pole, po wprowadzeniu tekstu należy zwalidować, tak aby spełniały następujące warunki:
    - imię i nazwisko ma zaczynać się od pierwszej litery, reszta z małych
    - numer telefonu, tylko ma się składać z samych cyfr oraz składać się z 9 cyfr jeśli jest to polski prefix, 10 lub 11 jeśli to niemiecki prefix, 10 jeśli jest to usa prefix
    - prefix musi być wybrany

## Panel 2
- jest to tabelka, która wyświetla następujące informacje:
    - Lp.
    - Imię
    - Nazwisko
    - Telefon (powinien zostać on połączony z prefixem)
- można zaznaczyć wiersz, który zostanie podswietlony
    - można odznaczyć wiersz, jeśli został on zaznaczony
    - tylko jeden wiersz może być zaznaczony
- nad tabelką, mają się pojawić przyciski akcji (jeśli wiersz jest zaznaczony, jeśli zostanie on odznaczony, to przyciski akcji mają zostać ukryte):
    - zadzwoń (jako przycisk, który zawiera ikonkę lub ikonka jako przycisk)
    - usuń (jako przycisk, który zawiera ikonkę lub ikonka jako przycisk)

## Dodatkowe informacje
- elementy mają być ostylowane z wykorzystaniem:
    - [Angular Bootstrap](https://ng-bootstrap.github.io/#/home)
    - [React Bootstrap](https://react-bootstrap.github.io/)

## Podglądowy layout
![](assets/zadanie_05_layout_preview.png)
